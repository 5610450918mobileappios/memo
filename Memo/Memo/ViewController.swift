//
//  ViewController.swift
//  Memo
//
//  Created by Teerapat on 11/16/2558 BE.
//  Copyright © 2558 Teerapat. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let db = Database.getSharedInstance()

    @IBOutlet weak var idTextField: UITextField!
    @IBOutlet weak var topicTextField: UITextField!
    @IBOutlet weak var contentTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func addButtonTouch() {
        let id = db.addNote(topicTextField.text!, content: contentTextView.text)
        self.idTextField.text = "\(id)"
        
    }
   
    @IBAction func loadButtonTouch() {
        let result = db.getNote(Int(self.idTextField.text!)!)
        
        topicTextField.text = result.topic
        contentTextView.text = result.content
    }


}

